# Zephyr: Pulse Width Modulation (PWM)

[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-pwm/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-pwm/-/commits/main)

* HTML of Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/Zephyr-PWM/Zephyr-PWM.html
* PDF of Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/Zephyr-PWM/Zephyr-PWM.pdf